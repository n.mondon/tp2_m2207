package tp_2;

public class TestForme extends Form{
	public static void main(String[] args){
		Form f1 = new Form (); //definition de F1
		Form f2 = new Form ("vert",false); // definition de F2
		System.out.println("F1 : "+f1.getColor() + " - " + f1.isColoriage()); // affichage F1	
		System.out.println("F2 : "+f2.getColor() + " - " + f2.isColoriage()); // affichage F2
		f1.setCouleur("red"); // redefinition de F2
		f1.setColoriage(false); // redefinition de F1	
		System.out.println("F1 bis: "+f1.getColor() + " - " + f1.isColoriage()); // affichage F1 bis
		System.out.println("************************************* Se Décrire **********************************");
		// F1 selon seDecrire()
		f1.seDecrir();
		// F2 selon seDecrire();
		f2.seDecrir();
		System.out.println("************************************* Cercle c1 **********************************");
		Cercle c1 = new Cercle();// definition de c1
		c1.seDecrir(); // c1 selon seDecrir()
		c1.getRayon();
		System.out.println("************************************* Cercle c2 **********************************");
		Cercle c2 = new Cercle(2.3);// definition de c2
		c2.seDecrir(); // c2 selon seDecrir()
		c2.getRayon();
		System.out.println("************************************* Cercle c3 **********************************");
		Cercle c3 = new Cercle(3.2,"jaune",false);// definition de c3
		c3.seDecrir(); // c3 selon seDecrir()
		c3.getRayon();
		System.out.println("************************************* Cylindre cy1 **********************************");
		Cylindre cy1 = new Cylindre();
		cy1.seDecrir();
		System.out.println("************************************* Cercle cy2 **********************************");
		Cylindre cy2 = new Cylindre(1.3,4.2,"bleu",true);// definition de c3
		cy2.seDecrir(); // cy2 selon seDecrir()
		cy2.getRayon();
		
		
	}
}
