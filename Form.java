package tp_2;

public class Form {
	String couleur;
	boolean coloriage;
	int nb_form = 0;
	
	public Form() {
		this.couleur = "orange";
		this.coloriage = true;
	}
	
	public Form(String c, boolean r) {
		this.couleur = c;
		this.coloriage = r;
	}
	
	public String getColor() {
		return couleur;
	}
	
	public void setCouleur(String c) {
		this.couleur = c;
	}
	
	public boolean isColoriage() {
		return coloriage;
	}
	
	public void setColoriage(boolean b) {
		this.coloriage = b;
	}
	
	public void seDecrir() {
		System.out.println("Cette forme a une couleur : "+ getColor() + " et son coloriage est : " + isColoriage());
		
	}
}
